#include "opencv\cv.h"
#include "opencv\highgui.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2\opencv.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <time.h>
#include <conio.h>
#include <iostream>
#include <tesseract\baseapi.h>
#include <leptonica\allheaders.h>


using namespace cv;
using namespace std;
#define spacebar 32


 int main( int argc, char** argv )
 {
    VideoCapture cap(0); //z�ap kamer�
    if ( !cap.isOpened() ) //sprawd�, czy poprawnie
    {
         return -1;			//je�li nie - wyjd�
    }

 Mat klatkaBufor;
 cap.read(klatkaBufor); 

 Mat linia = Mat::zeros( klatkaBufor.size(), CV_8UC3 );
 
 int polozenieX = -1; 
 int polozenieY = -1;

    for(;;)
    {
      Mat klatka, klatkaHSV, klatkaProgowa, klatkaKontur;
      cap.read(klatka);
	cvtColor(klatka, klatkaHSV, COLOR_BGR2HSV);

	 inRange(klatkaHSV, Scalar(50, 100, 100), Scalar(80, 255, 255), klatkaProgowa); 

  Moments Momenty = moments(klatkaProgowa);
  double M01 = Momenty.m01;
  double M10 = Momenty.m10;
  double M00 = Momenty.m00;

  //wyliczenie centroidu
	int pozycjaPilkaX = M10/M00;
    int pozycjaPilkaY = M01/M00;        
        
   if (polozenieX >= 0 && polozenieY >= 0 && pozycjaPilkaX >= 0 && pozycjaPilkaY >= 0)
   {
    line(linia, Point(pozycjaPilkaX, pozycjaPilkaY), Point(polozenieX, polozenieY), Scalar(0,255,0), 5);
   }

   polozenieX = pozycjaPilkaX;
   polozenieY = pozycjaPilkaY;
  
   
 // imshow("Klatka progowa", klatkaProgowa);
  klatkaKontur=linia;
  klatka = klatka + linia;
  flip(klatka, klatka, 1);;
  
  imshow("Klatka", klatka); 
 // imshow("Klatka kontur", klatkaKontur); 

        if (waitKey(30) == 27) 
       {
            break; 
       } 
		else if(waitKey(30) == 32)
		{
			flip(linia, linia, 1);
			imwrite("test.jpg", linia);
			Mat obrobka = imread("test.jpg");
			cvtColor(obrobka, obrobka, COLOR_BGR2GRAY);
			imwrite("obrobiony.png", obrobka);
			Mat obrobiony = imread("obrobiony.png") ;
			imshow("Obrobiony obraz", obrobiony);
//tesseract
tesseract::TessBaseAPI *myOCR = //inicjalizacja instancji
    new tesseract::TessBaseAPI();

  if (myOCR->Init(NULL, "eng")) {
    fprintf(stderr, "Problem z inicjalizacja");
    exit(1);
  }

  tesseract::PageSegMode pagesegmode = static_cast<tesseract::PageSegMode>(7); //7-pojedynczy blok tekstu
  myOCR->SetPageSegMode(pagesegmode);

  Mat obraz = imread("obrobiony.png", 0);
  Rect litera(1, 1, 520, 420);

  myOCR->TesseractRect( obraz.data, 1, obraz.step1(), litera.x, litera.y, litera.width, litera.height); //obraz, bity na piksel, bity na linie
  const char *litera2 = myOCR->GetUTF8Text();
  printf("Litera: \n");
  printf(litera2);
  printf("\n");

  waitKey(0);
   if (waitKey(30) == 27) 
       {
            break; 
       }
		}
	}
	
   return 0;
}